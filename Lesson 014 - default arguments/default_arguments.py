#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Follow the examples after if __name__ == "__main__": to understand Python objects.
# Other comments for the class and methods examples are just to support the examples
# in the main section.
#

# Test class demonstrates how mutable default arguments work.
class Test:
    def __init__(self, list_value = [], value_to_append = 1):
        self._list_value = list_value
        self._list_value.append(value_to_append)

    ## Return the values as a string, when function str is used on the Test object.
    def __str__(self):
        return str(self._list_value)

    def append(self, value):
        self._list_value.append(value)

# Test2 class demonstrates how to avoid mutable objects being shared between all methods.
class TestDefaultNone:
    def __init__(self, list_value = None, value_to_append = 1):
        if list_value is None:
            self._list_value = []
        self._list_value.append(value_to_append)

    ## Return the values as a string, when function str is used on the Test object.
    def __str__(self):
        return str(self._list_value)

    def append(self, value):
        self._list_value.append(value)

# Start the reading of the lesson from this point, to understand the use of the variables
# with methods and class objects.
if __name__ == "__main__":

    # The examples of using the Test class demonstrate an unexpected side effect when using
    # mutable default values like [] within the Test.__init__ method.
    # Note default immutable values do not experience the same behavior,
    # and work as the same default value each time they are used.
    # The following examples demonstrates the unexpected behavior of mutable default values like [].

    # The test object will be created and the test._list_value list will initially be set to [1].
    test = Test()
    print(str(test))

    # After appending 2 to the test2 list, it would be expected that the value would be [1, 2].
    # The list stored within test2._list_value is actually set to [1, 1, 2] after then append.
    # This is because any default value that is a mutable value is created at the time of definition.
    # The default value is then shared for all self._list_value for all Test objects that use it.
    # The test variable updated the default value to [1], and then this has been re-used for test2
    # to set test2._list_value to the default value, which is now [1], then append 1 and 2.
    test2 = Test()
    test2.append(2)
    print(str(test2))

    # To prevent this behavior, you can set the default to None, then only set self._list_value
    # to an empty list after checking list_value argument for None.
    # Now it's possible to see that test will print [1] and test2 will print [1, 2].
    test = TestDefaultNone()
    print(str(test))

    test2 = TestDefaultNone()
    test2.append(2)
    print(str(test2))

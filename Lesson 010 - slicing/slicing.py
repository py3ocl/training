#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":

    # Lists and tuples can sliced, which allows extraction of elements from a list or tuple.
    list_of_values = [1, 2, 3, 4]
    print(list_of_values)

    # Lists and tuples can be sliced using a start, end and step.
    # The following example explicitly gets all elements from the list.
    # Note that when the start is omitted it will default to 0,
    # when the end is omitted it will default to the size of the list or tuple,
    # and when the step is omitted it will default to 1.
    # If the step is a negative value, then this is applied
    # from the end towards the start of the list or tuple.
    print(list_of_values[0:4:1])

    # Get all remaining values from a list or tuple from a starting index.
    # This example will extract [2, 3, 4].
    print(list_of_values[1:])

    # Specify a range, with this example extracting [2, 3].
    print(list_of_values[1:3])

    # Get all remaining values from the end,
    # with this example extracting the last two elements, [3, 4].
    print(list_of_values[-2:])

    # Specify a range from the end, with this example extracting [1, 2].
    print(list_of_values[-4:-2])

    # Get all the values from the start while skipping every other value,
    # with this example getting [1, 3].
    print(list_of_values[0::2])

    # The list can be extracted in reverse order, which will be [4, 3, 2, 1].
    print(list_of_values[::-1])

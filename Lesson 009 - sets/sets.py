#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":

    # Create an empty set.
    set_of_values = set()

    # Sets store unique values and can be initialised to values from a tuple or list.
    set_of_values = set((1, 2, 3))
    print(f"Initial set values: {set_of_values}")

    # To find if value(s) exist within the set, this can be tested using an intersection.
    if set_of_values.intersection((1,)):
        print("set_of_values contains 1")

    # Two sets can be combined using a union.
    combined_set_of_values = set_of_values.union(set((4,5,6)))
    print(f"Union of two sets: {combined_set_of_values}")

    # Alternative method for union of two sets.
    combined_set_of_values = set_of_values | set((4,5,6))
    print(f"Union of two sets: {combined_set_of_values}")

    # It's also possible to get the difference between two sets.
    differences = combined_set_of_values.difference(set_of_values)
    print(f"Difference of two sets: {differences}")

    # Alternative method for difference of two sets.
    differences = combined_set_of_values - set_of_values
    print(f"Difference of two sets: {differences}")

    # The set can be combined with other sets.
    set_of_values.update(set((7,8)))
    print(f"Combined set: {set_of_values}")

    # Intersect two sets.
    set1 = set((1, 2, 4))
    set2 = set((2, 3, 4))
    set_intersection = set1.intersection(set2)
    print(f"Intersection of two sets: {set_intersection}")

    # Erase an element from a set.
    set_intersection.discard(2)
    print(f"Remove a value from a set: {set_intersection}")

#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# When __name__ is equal to "__main__" then this file has been run from command line.
# More about the if statement and conditions are covered later.
# Python uses space or tab characters to signify indentation, and it's recommended to use
# 4 space characters for each level of indentation.
if __name__ == "__main__":

    # Python has in-built methods, such as print.
    # Providing brackets is required to call a function,
    # and some functions such as print allow arguments (or values) to be supplied.
    print("Hello World")

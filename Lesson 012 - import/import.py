#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Import all objects from sys.
import sys

# Only import the python_version method from platform.
from platform import python_version

# Only import the python_version method from platform and
# create the alias name pv for python_version method.
from platform import python_version as pv

if __name__ == "__main__":

    # Print full version information include date and platform information.
    # Note that imported libraries have functions or values which can be accessed using . (dot)
    # e.g. 3.7.3 (v3.7.3:ef4ec6ed12, Mar 25 2019, 22:22:05) [MSC v.1916 64 bit (AMD64)]
    print(sys.version)

    # Print only the Python version number.
    # e.g. 3.7.3
    print(python_version())
    print(pv())

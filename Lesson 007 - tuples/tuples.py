#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":

    # Tuples cannot be modified once created, but can perform faster than a list.
    tuple_of_values = (1, 2, 3)
    print(tuple_of_values)

    # Values can be indexed for reading the value only.
    print(tuple_of_values[0])

    # Test if a value exists within the tuple.
    if 2 in tuple_of_values:
        print("tuple_of_values contains 2")

    # Get number of elements within the tuple.
    print("number of elements: " + str(len(tuple_of_values)))

    # Tuples can be re-assigned.
    tuple_of_values = tuple_of_values + (3, 4, 5, 6)
    print(tuple_of_values)
    print('count of value 3 within tuple is ' + str(tuple_of_values.count(3)))

    # If you need to have a tuple with one element,
    # it must end with a comma before the closing bracket.
    tuple_of_values = (1,)
    print(tuple_of_values)

    tuple_of_values = (1, 2, 3)
    # Unpack values from tuples into variables a, b and c.
    a, b, c = tuple_of_values
    print(str(a) + " " + str(b) + " " + str(c))

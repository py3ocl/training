#!/usr/bin/env python

# Copyright 2023 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Python default arguments of object types will be bound to the parameters of a function.

# Example of a function using an object type for a default value.
# The default object will be bound to the values parameter, when a default is not specified.
def append4bound(values = [1, 2, 3]):
    values.append(4)
    print(values)

# Example of working around the bound default object values for function parameters.
# By using None and then setting the value later, this will avoid a default object being bound to a parameter.
def append4unbound(values = None):
    if values is None:
        values = [1, 2, 3]
    values.append(4)
    print(values)

#
# Following examples of using append4bound and append4unbound
#

# This will print:: [1, 4]
append4bound([1])

# This will print: [1, 2, 3, 4]
append4bound()

# Notice that values within append4bound is now bound to the original default.
# This will now print: [1, 2, 3, 4, 4]
append4bound()

# This will print: [1, 2, 4]
append4bound([1, 2])

# Notice the original binding remains.
# This will now print: [1, 2, 3, 4, 4, 4]
append4bound()


# Both will print: [1, 2, 3, 4]
append4unbound()
append4unbound()

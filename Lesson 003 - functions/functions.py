#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Python provides guidance which is documented for the community,
# known as Python Enhancement Proposals or PEP for short.
# These PEPS are numbered, and PEP 8 provides guidance on how to name
# variables, methods, etc.
# The following methods use the naming convension recommended by Python PEP 8.

# Define a function that does not take any arguments.
# Indentation is required after the line containing the def statement.
def print_hello():
    print("Hello")

# Define a function that takes one argument.
def print_value(value):
    print(value)

def add_values(integer_value1, integer_value2):
    return integer_value1 + integer_value2

if __name__ == "__main__":

    print_hello()
    print_value(1)
    print_value("Goodbye")
    print(add_values(2, 3))

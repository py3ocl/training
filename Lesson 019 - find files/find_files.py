#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

if __name__ == "__main__":

    print("List files within test_dir folder")
    for f in os.listdir("."):
        print(f)
    print("")

    # Print all the files and folders with a relative path to the search directory.
    print("Recursively list all files and folders:")
    for root, dirs, files in os.walk("."):

        # Don't want to display path when it's "."
        curpath = os.path.relpath(root) if root != "." else ""
        if curpath:
            print(curpath)

        for f in files:
            # Can only get the relative path when curpath is not empty.
            relpath = os.path.relpath(curpath) if curpath else ""

            # Join the relative path and the filename.
            # When the relative path is empty, only the filename is displayed. 
            print(os.path.join(relpath, f))

#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":

    # Create an empty list.
    list_of_values = []

    # Append the value 1 to the empty list.
    list_of_values.append(1)

    # Append the values 2 and 3 to the list.
    list_of_values += [2, 3]
    print(list_of_values)

    # Replace existing list of [1, 2, 3] with ["cd"].
    list_of_values = ["cd"]

    # Insert value "ab" to the start of the list.
    list_of_values.insert(0, "ab")

    # Append the value "ef" to the list.
    list_of_values.append("ef")

    # Append the values "gh" and "ij" to the list.
    # The list will contain ["ab", "cd", "ef", "gh", "ij"].
    list_of_values += ["gh", "ij"]
    print(list_of_values)

    # Index the second element from the beginning of the list, which is "cd".
    print(list_of_values[1])

    # Index the element at the end of the list, which is "ij".
    print(list_of_values[-1])

    # Index the second from last element of the list, which is "gh".
    print(list_of_values[-2])

    # Remove the second element from the list.
    del list_of_values[1]

    # Test if value "ab" exists within the list.
    if "ab" in list_of_values:
        print('list_of_values contains "ab"')

    # Get number of elements within the tuple.
    print("number of elements: " + str(len(list_of_values)))

    # Find and remove the matching value "ij".
    list_of_values.remove("ij")
    print(list_of_values)

    # Append "ab" to the list then get number of elements containing value "ab".
    list_of_values.append("ab")
    print(list_of_values)
    print('count of "ab" within list is ' + str(list_of_values.count("ab")))

    # Values can be indexed by position and the value can be modified at the position.
    list_of_values[0] = "AB"
    print(list_of_values)

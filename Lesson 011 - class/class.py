#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Define an address that provides common properties of an address,
# and a basic set of methods that allow the address to be queried or changed.
# Keeping values and methods together within a class allows the relationship
# between properties and the operations to be managed together.
# The ability to manage the properties and operations within a class allows us to
# encapsulate the behaviour and is part of object oriented programming.
class Address:

    # The __init__ method is automatically called when the class object is created.
    # For any method to access the values on a class objet, the need to specify
    # self as the first argument of the method.
    def __init__(self, house_number, street, city, postcode):
        self.set_address(house_number, street, city, postcode)

    # Allow str method to convert the Address object into a string.
    def __str__(self):
        return str(self.get_address())

    # Get the address as a tuple of strings.
    def get_address(self):
        return (self._house_number, self._street, self._city, self._postcode)

    def set_address(self, house_number, street, city, postcode):
        # By using self. before each value,
        # this is adding the value to the class object as it's value is set.
        # The variable names for the class start with underscore, to signify these
        # values are only intended to be used internally by the class,
        # and is part of the Python recommendation, which can be found in PEP 8.
        self._house_number = house_number
        self._street = street
        self._city = city
        self._postcode = postcode

    def print_address(self):
        # Iterate through the list returned by the get_address function, outputting each line.
        for addr_line in self.get_address():
            print(addr_line)

if __name__ == "__main__":

    # Create the object containing the address details for the UK Priminister.
    addr = Address(10, "Downing Street", "London", "SW1A 2AA")
    print("Address of Priminister:")
    addr.print_address()

    # Change the address to the UK Chancellor.
    addr.set_address(11, "Downing Street", "London", "SW1A 2AB")
    print("\nAddress of Chancellor:")
    addr.print_address()

    # Print the string returned by __str__ method.
    print("\nString from addr.__str__:")
    print(str(addr))

    # Note that all the writable attributes of a class are stored within the __dict__ dictionary,
    # such as _house_number, _street, _city and _postcode.
    print("\nObjects attributes from addr.__dict__:")
    print(addr.__dict__)

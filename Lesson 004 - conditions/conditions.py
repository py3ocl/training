#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":

    # Change the age to see the conditions change the output.
    age = 22

    # Indentation is required after the if, elif and else statements.
    if age < 16:
        print("age " + str(age) + ". So young!")
    elif age < 30:
        if age >= 16 and age <= 24:
            print("age " + str(age) + ". So young and so much more to learn")
        else:
            print("age " + str(age) + ". So much more to learn")
    elif age < 40:
        print("age " + str(age) + ". Now we're getting started")
    else:
        print("age " + str(age) + ". Is there anything else to learn?")

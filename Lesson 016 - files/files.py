#!/usr/bin/env python

# Copyright 2024 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

if __name__ == "__main__":

    # Create a text file with text lines.
    # Note that "\n" on Windows will be converted to "\r\n"

    lines = ["Line 1", "Line 2"]

    filename = "test.txt"

    f = open(filename, "w")
    for line in lines:
        f.write(line + "\n")
    f.close()

    # Read the file and display the lines in for loop.
    f = open(filename, "r")
    for line in f:
        print(line, end="")
    f.close()

    # Read the file, display the lines in for loop and automatically close the file.
    with open(filename, "r") as f:
        for line in f:
            print(line, end="")

    # Read the file and display the lines using readline method.
    with open(filename, "r") as f:
        line = " "
        while len(line) > 0:
            line = f.readline()
            if line:
                print(line, end="")

    # Delete test.txt file.
    os.remove(filename)

    # Create the binary file with Unix line endings "\n", even when run on Windows.
    with open(filename, "wb") as f:
        for line in lines:
            f.write((line + "\n").encode("ascii"))

    # Read the binary file and display the lines using readline method.
    with open(filename, "rb") as f:
        line = " "
        while len(line) > 0:
            line = f.readline()
            if line:
                print(line.decode(), end="")

    # Delete test.txt file.
    os.remove(filename)

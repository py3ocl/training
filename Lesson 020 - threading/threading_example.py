#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Import the Python libraries for managing a thread and safely locking access to data between threads.
from threading import Thread
from threading import Lock

# Define a class that guarantees the count is incremented safely between threads.
class ThreadSafeCounter:

    # Setup the mutex and count.
    def __init__(self):

        # The shared count used between threads.
        self._count = 0

        # A mutex for incrementing a count safely in a thread.
        self._mutex = Lock()

    # Increment the count and return the newly incremented value.
    def inc(self):

        # Increment the count exclusively within the current thread.
        # The mutex will be prevent other threads from accessing self._count
        with self._mutex:
            self._count += 1

            # Take a copy of self._count while access is limited to the current thread.
            # This will guarantee the returned count is local and not modified by other threads.
            count = self._count

        # Return the last safely incremented count.
        return count

# Set a mutex used for printing a value in a thread safely.
printMutex = Lock()

# Print a value within a thread safely.
def threadSafePrint(value):

    # Any global variable must be marked as global to be accessible from a function.
    global printMutex

    # Limit only one thread to printing a value.
    with printMutex:
        print(value)

# Define a custom thread class that can increment and display a count.
class MyThread(Thread):

    def __init__(self, thread_name, counter):
        Thread.__init__(self)

        # Keep track of the thread name for display.
        self._thread_name = thread_name

        # Store a reference to the shared counter for each thread.
        self._counter = counter

    # Threading function.
    def run(self):

        # Safely increment the count for the currently running thread and get the latest count.
        count = self._counter.inc()

        while count < 100:

            # Safely print the thread name and count in the currently running thread.
            message = "{}: {}".format(self._thread_name, count)
            threadSafePrint(message)

            # Safely increment the count for the currently running thread and get the latest count.
            count = self._counter.inc()

# Run the threading example when run from a terminal or console.
if __name__ == "__main__":

    # Create the counter to be shared between threads.
    counter = ThreadSafeCounter()

    # Create two threads that will be used to increment and display the count.
    thread1 = MyThread("thread1", counter)
    thread2 = MyThread("thread2", counter)

    # Start both threads.
    thread1.start()
    thread2.start()

    # Wait for both threads to complete.
    thread1.join()
    thread2.join()

    # Display the Complete message once the two threads have ended.
    print("Complete")

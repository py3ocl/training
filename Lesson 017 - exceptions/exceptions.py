#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

if __name__ == "__main__":

    # s = "Hello"

    # Demonstrate an exception when a variable is not defined.
    # NOTE: Removing the comment for the variable s will show the variable s is displayed.
    try:
        print(s)
    except NameError:
        print("Variable not defined")
    except:
        print("Unknown exception")
    else:
        print("No exception: " + s)

    # Demonstrate an exception when a file is not defined.
    # NOTE: Creating test.txt with a line of text will show file contents are displayed.
    try:
        f = open("test.txt", "r")
        s = f.readline()
        print(s, end="")
        f.close()
    except OSError:
        print("File not found")
    except:
        print("Unknown file exception")
    else:
        print("No file exception: " + s)

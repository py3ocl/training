#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":

    # Basic example of setting a string, which can be used with either quotes.
    s = "Hello1"
    print(s)

    s = 'Hello2'
    print(s)

    # Set the string with some values and print "1 is one".
    s = "{} is {}".format(1, "one")
    print(s)

    age = 22
    name = "Jane"

    # Print "Jane is 22 years old" by using arguments in order provided.
    s = "{} is {} years old".format(name, age)
    print(s)

    age = 24
    name = "Bob"

    # Print "Bob is 24 years old" by using the 2nd argument then 1st argument.
    s = "{1} is {0} years old".format(age, name)
    print(s)

    # Print "James is 41 years old" by assigning a key to a variable for each argument.
    age = 41
    name = "James"
    print("{n} is {a} years old".format(n=name, a=age))

    # Print "Sarah is 37 years old" by placing variables within the string and using f"" syntax.
    age = 37
    name = "Sarah"
    print(f"{name} is {age} years old")

    # Print a list containing "1" and "2".
    print("1,2".split(","))

    # Print string "1 2".
    print(" ".join("1,2".split(",")).strip())

    # Set the path without the need to escape the character and print.
    p = r"C:\Program Files"
    print(p)

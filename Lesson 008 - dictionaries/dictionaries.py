#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":

    # A dictionary provides the ability to look up a value based on a unique key.
    # Initialise the dictionary with two key/value pairs.
    # Dictionaries are ordered by the key.
    dict_of_values = {1:"one", 2:"two"}

    print("The word for 1 is " + dict_of_values[1])
    print("The word for 2 is " + dict_of_values[2])

    # Add the value for the key 4.
    dict_of_values[4] = "four"
    print("The word for 4 is " + dict_of_values[4])

    # It's possible to test for a key within a dictionary.
    if not dict_of_values.get(3):
        print("There is no value for key 3")

    # It's possible to remove a key and get the removed value.
    # The key/value pair for 2 is removed here, and "two" is displayed.
    print(dict_of_values.pop(2))

    # Print the remaining key/value pairs, which will be for 1 and 4.
    print(dict_of_values)

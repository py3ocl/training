# Training

![](header_image.jpg)

Python 3 training material.

# Download instructions

To download from command prompt type the following:  
mkdir py3ocl  
cd py3ocl  
git clone https://gitlab.com/py3ocl/training

# Git install instructions (Windows)

To install the Git tool on Windows, you can install TortoiseGit from https://tortoisegit.org/

# Git install instructions (Linux)

To install the Git tool on Linux, type command: sudo apt install git

# Example of running a lesson from Windows command prompt

```
cd "py3ocl\training\Lesson 001 - Hello World"  
hello_world.py
```

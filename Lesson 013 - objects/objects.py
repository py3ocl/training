#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Follow the examples after if __name__ == "__main__": to understand Python objects.
# Other comments for the class and methods examples are just to support the examples
# in the main section.
#

# Test class for demonstrating how to set the a and b properties directly,
# or setting the properties via the set_ab method.
class Test:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    ## Return the values as a string, when function str is used on the Test object.
    def __str__(self):
        return "{" + str(self.a) + ", " + str(self.b) + "}"

    def set_ab(self, a, b):
        self.a = a
        self.b = b

# Example of setting the Test class a and b properties directly on the Test class object.
def set_test_values_directly(test, a, b):
    test.a = a
    test.b = b

# Example of setting the Test class a and b properties via the set_ab method on the Test class object.
def set_test_values_via_method(test, a, b):
    test.set_ab(a, b)

# Generic example of showing how a method cannot change the value outside the method call.
def try_set_value(value, new_value):
    value = new_value

# Example of changing an value in a list and appending a new value in the same list.
def update_list(lst):
    lst[0] = 11
    lst.append(14)

# Print the values, type and identified of a variable named value.
def print_properties(value):
    print("value is " + str(value) +
          " with type " + str(type(value)) +
          " and identifier " + hex(id(value)))

# Start the reading of the lesson from this point, to understand the use of the variables
# with methods and class objects.
if __name__ == "__main__":

    # This lesson can be followed without reading the Python documentation,
    # but reading the following two links will provide an in-depth understanding
    # of how Python manages objects and how name binding works.
    # There is no need to read the documentation before completing this lesson.
    #
    # Python describes how all data is managed here:
    # https://docs.python.org/3/reference/datamodel.html
    # Python binds names to objects, which can be read here:
    # https://docs.python.org/3/reference/executionmodel.html

    #
    # The following documentation with code examples is provided
    # to aid in the understanding of how Python objects are used and behave.
    #
    # NOTE: Name binding is not mentioned in this lesson, as this lesson focuses
    # on the behavior of data and not naming, with the exception of re-using a variable name.
    #

    # Some types are immutable, which means you cannot modify the object value.
    #
    # The immutable variable names can be re-used to assign a new value,
    # meaning the old value is discarded.
    #
    # Immutable types include boolean, integer, float, decimal, string and tuple.
    #
    # Mutable types include list, dictionary, set and user defined classes.

    # All data in Python are objects, which have a value, type and identity.
    integer_value = 1
    print_properties(integer_value)

    # By assigning integer_value to a new value,
    # this replaces the old object with a new object with the value 2.
    # This can be verified by observing that the id has changed after the assignment.
    integer_value = 2
    print_properties(integer_value)

    # Variables references are passed into functions, but changing the values inside
    # the function won't affect the value of the variables outside the function.
    # Notice that integer_value will still have the value 2 after the call to try_set_value.
    try_set_value(integer_value, 3)
    print_properties(integer_value)

    # Even though a class type is mutable, it is not possible to set the object
    # to a new object within a method.
    test_value = Test(1, "One")
    print_properties(test_value)
    try_set_value(test_value, Test(2, "Two"))

    # It can be seen that test_value has not been modified after using try_set_value method.
    print_properties(test_value)

    # It is possible to change the values of a mutable type through the
    # methods of the objects, or by accessing the properties on the object,
    # when passed into a method, such as method set_test.
    # This example sets the values a and b on the Test object directly within
    # method set_test_values_directly.
    set_test_values_directly(test_value, 3, "Three")
    print_properties(test_value)

    # This example will set values a and b via the Test method set_ab
    # within method set_test_values_via_method.
    set_test_values_via_method(test_value, 4, "Four")

    list_value = [1, 2, 3]
    print_properties(list_value)

    # The method update_list can modify a list using [] and append methods on the list variable.
    # The list will be modified and contain [11, 2, 3, 14].
    update_list(list_value)
    print_properties(list_value)

    # The None object is a pre-defined variable of type NoneType with a value None.
    # Variables can be set to None, meaning the variable references the build-in None variable.
    none_value = None

    # When comparing against None, the is keyword should be used to compare that the reference
    # to None is the same (do not use == comparison for None types, as recommended by PEP 8).
    if none_value is None:
        print("none_value is None type")

    # The none_value variable is referencing the None built-in variable.
    # This can be verified by comparing the identifiers of none_value and None.
    if id(none_value) == id(None):
        print("id(none_value) == id(None)")

    # It's also possible to see the value, type and identifier for none_value and None,
    # which in this example show the same information for both.
    print_properties(none_value)
    print_properties(None)

    # The variable which is of None type can be assigned a different value, which will
    # set the variable to a new type.
    # The following example sets none_value to a value of 1 and type to integer.
    none_value = 1
    print_properties(none_value)

    # The assignment of new_integer_value to none_value sets new_integer_value to the
    # same reference as none_value, which means they both have the same identifier.
    new_integer_value = none_value

    # Deleting none_value removes the ability to use the none_value, but new_integer_value
    # still retains the same identifier.
    del none_value

    # It's possible to see that new_integer_value still has the
    # same properties as none_value properties printed previously.
    print_properties(new_integer_value)

    # It's possible to identify variable types.
    if type(integer_value) is int:
        print("integer_value is int")

    if type(new_integer_value) is int:
        print("new_integer_value is int")

    if type(list_value) is list:
        print("list_value is list")

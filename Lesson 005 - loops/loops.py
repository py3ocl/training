#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":

    # Use the in-built function range to start at 1
    # and go up to 4.
    # 1, 2 and 3 will be displayed.
    print("for loop:")
    for i in range(1, 4):
        print(i)

    # Loop between 1 and 3 using the while loop.
    print("while loop:")
    i = 1
    while i < 4:
        print(i)
        i += 1
 
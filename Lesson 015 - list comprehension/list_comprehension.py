#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":

    list_values = [1, 2, 3, 4, 5, 5]

    # The list comprehension allows for lists to be iterated and manipulated in a single statement.
    # This example sets a mew list from the current list, where a condition is met for each item.
    even_values = [value for value in list_values if value % 2 == 0]
    print(str(even_values))

    # Each value can also be manipulated in place, such as multiplying by 2.
    multiplied_values = [value * 2 for value in list_values]
    print(str(multiplied_values))

    # Use list comprehension to take the values in the matrix and create a new matrix
    # containing 1 or 0 for odd or even values.
    matrix = [ [1, 2],
               [4, 5],
               [7, 8] ]

    odd_and_even_matrix = [ [ 1 if v % 2 else 0 for v in matrix[row] ] for row in range(len(matrix)) ]
    print(str(odd_and_even_matrix))
